#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int count;
int map[7][7] =
{
	{ 0, 0, 0, 1, 0, 0, 0 },
	{ 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 1, 0, 0, 0, 1, 0 },
	{ 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 1, 0, 1, 0, 1, 0 },
	{ 0, 1, 0, 0, 0, 1, 0 }
};

void labir(int startx, int starty, int labwidth, int labheight){

	if ((startx < labwidth) && (startx >= 0) && (starty < labheight) && (starty >= 0)){
		map[startx][starty] = 5;
		if ((startx == 6) && (starty == 0)){
			printf("Wyjscie znalezione.\n");
		}
		else if ((starty + 1) < labheight && map[startx][starty + 1] == 0){

			printf("Nastempny krok (y+).\n");
			labir(startx, starty + 1, labwidth, labheight);

		}
		else if ((startx - 1) >= 0 && map[startx - 1][starty] == 0){

			printf("Nastempny krok (x-).\n");
			labir(startx - 1, starty, labwidth, labheight);

		}
		else if ((starty - 1) >= 0 && map[startx][starty - 1] == 0){

			printf("Nastempny krok (y-).\n");
			labir(startx, starty - 1, labwidth, labheight);

		}
		else if ((startx + 1) < labheight && map[startx + 1][starty] == 0){

			printf("Nastempny krok (x+).\n");
			labir(startx + 1, starty, labwidth, labheight);

		}
		else{
			printf("Wyjscia nie znaleziono.\n");
		}
	}
}




typedef struct Stoss{

	int dane;
	struct Stoss *poprzedni;

}Stos;

Stos *Ws = NULL, *Wp = NULL, *Wk = NULL;


Stos* Stos_init(){
	Stos *nowy_stos = (Stos*)calloc(1, sizeof(Stos));

	nowy_stos->poprzedni = NULL;
	nowy_stos->dane = 0;

	return nowy_stos;
}


void push(Stos **st, int c){
	Stos *ph;
	Stos *pt;

	if (((*st)->dane == '\0') || ((*st)->dane == ' ')){

		(*st)->dane = c;

	}
	else {

		ph = *st;
		pt = (Stos*)calloc(1, sizeof(Stos));
		*st = pt;
		(*st)->dane = c;
		(*st)->poprzedni = ph;

	}

}


void pisz_stos(Stos *st){

	if (((st == NULL) || (st->dane == 0))){

		printf("Stos Pusty", '\n');

	}
	else{

		while (st->poprzedni != NULL){

			printf("%i", st->dane);
			st = st->poprzedni;

		}

		printf("%i", st->dane);

	}

}


int pop(Stos **st){
	int c;

	if ((*st)->poprzedni != NULL){

		Stos *ph = NULL;
		c = (*st)->dane;
		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;
		return c;

	}
	else{

		c = (*st)->dane;
		(*st)->dane = 0;
		return c;

	}

}


int peek(Stos *st){
	int c;

	if (st->dane == 0){

		return 0;

	}
	else{

		c = st->dane;
		return c;

	}

}


bool isEmpty(Stos *st){
	int i = 0;

	if (st->dane != 0){

		i++;

	}

	if (i > 0){

		return false;

	}
	else{

		return true;

	}

}


size_t size(Stos *st){
	size_t i = 0;

	if (((st->dane == 0) && (st->poprzedni == NULL))) {

		return i;

	}

	while (st->poprzedni != NULL){

		i++;
		st = st->poprzedni;

	}

	i++;

	return i;

}


void niszcz(Stos **st){
	Stos *ph;

	while ((*st)->poprzedni != NULL){

		ph = (*st)->poprzedni;
		free(*st);
		*st = ph;

	}

	if ((*st)->poprzedni == NULL){

		(*st)->dane = 0;

	}

}


void Hanoi(int start, int cel, int n)
{
	Stos *zrodlo = NULL, *koniec = NULL;


	int c = 6 - start - cel;
	

	switch (start){
		case 1:{
			zrodlo = Ws;
			break;
			}
		case 2:{
			zrodlo = Wp;
			break;
			}
		case 3:{
			zrodlo = Wk;
			break;
		}
	}

	switch (cel){
		case 1:{
			koniec = Ws;
			break;
		}
		case 2:{
			koniec = Wp;
			break;
		}
		case 3:{
			koniec = Wk;
			break;
		}
	}

	if (n == 1){

		
		if (peek(zrodlo) == -1){
			return;
		}

		int object = pop(&zrodlo);

		push(&koniec, object);

		switch (start){

			case 1:{
				Ws = zrodlo;
			}
			case 2:{
				Wp = zrodlo;
			}
			case 3:{
				Wk = zrodlo;
			}
			default:{
				break;
			}

		}
		switch (cel){

			case 1:{
				Ws = koniec;
			}
			case 2:{
				Wp = koniec;
			}
			case 3:{
				Wk = koniec;
			}
			default:{
				break;
			}

		}
		
		count++;

		if (count > 20){
			system("pause");
			system("cls");
			count = 0;
		}

		printf("\tPierwsza Wieraza:\n");
		pisz_stos(Ws);
		printf("\n\tDruga Wieraza:\n");
		pisz_stos(Wp);
		printf("\n\tTrzecia Wieraza:\n");
		pisz_stos(Wk);
		printf("\n\n");
		
	}
	else{
		Hanoi(start, c, (n - 1));
		Hanoi(start, cel, 1);
		Hanoi(c, cel, (n - 1));

	}
}



main(){
	

	Ws = Stos_init();
	Wp = Stos_init();
	Wk = Stos_init();

	int n = 5;

	

	labir(6, 6, 7, 7);
	for (int i = 0; i < 7; i++){

		printf("\t%i %i %i %i %i %i %i \n", map[i][0], map[i][1], map[i][2], map[i][3], map[i][4], map[i][5], map[i][6]);

	}


	for (size_t i = n; i > 0; i--)
	{
		push(&Ws, i);
	}

	printf("\tPierwsza Wieraza:\n");
	pisz_stos(Ws);
	printf("\n\tDruga Wieraza:\n");
	pisz_stos(Wp);
	printf("\n\tTrzecia Wieraza:\n");
	pisz_stos(Wk);
	printf("\n\n");

	Hanoi(1, 3, n);

	niszcz(&Ws);
	niszcz(&Wp);
	niszcz(&Wk);

	free(Ws);
	free(Wp);
	free(Wk);

	printf("\n\n");
	printf("\n\n");
	
	system("pause");
	return 0;
}